const { query } = require('express');
var express = require('express');
var router = express.Router();
let VoituresModel = require("../model/VoitureModel");

router.get('/', function(req, res, next) {
      res.render('index', { title: "des chars" });
});
/**read */
router.get('/voitures', function(req, res, next) {
    VoituresModel.find({}).then(function(document){
      res.render('voiture', { voiture: document });
    }
  );
});
/**Create */
router.post("/voiture", function(req, res, next){
  let voiture_model = new VoituresModel(req.body);
  voiture_model.save().then(voiture_model =>{
    res.redirect("/voitures");
  })
});
/* delete*/
router.get("/voitures/delete/:id", function(req,res,next){
  let voiture_model = new VoituresModel();
  VoituresModel.findByIdAndRemove(req.params.id).then(function(err){
  if(err){
    console.log(err)
      res.redirect("/voitures");
  } else {
      res.redirect("/voitures");}
  });
});

/***Update Read */
router.get('/voitures/update/:id', function(req, res, next) {
  VoituresModel.findById(req.params.id).then(
    function(document){
      res.render('update', { voiture: document });
    }
  );
});
/**Update */
router.post('/voitures/update/:id', function(req, res, next) {
  VoituresModel.updateOne({_id:req.params.id},req.body).then(
    function(document){
      res.redirect("/voitures");
    }
  );
});

module.exports = router;
