var mongoose = require('mongoose');
const Schema = mongoose.Schema;

let VoitureSchema = new Schema({
    marque: String,
    annee: String,
    accident: String
}, {
    timestamps:true
});


module.exports = mongoose.model("Voiture", VoitureSchema);